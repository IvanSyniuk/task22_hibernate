package com.model;

import javax.persistence.*;
import lombok.*;

import java.util.Set;

@Entity
@Table(name = "modul")
@Getter
@Setter
public class Modul {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int Id;
    @Column(name = "name")
    private String name;
    @OneToMany(cascade = CascadeType.ALL,mappedBy = "modul")
    private Set<Test> tests;
}
