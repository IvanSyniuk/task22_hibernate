package com.model;

import javax.persistence.*;
import lombok.*;

import java.util.Set;

@Entity
@Table(name = "week")
@Getter
@Setter
public class Week {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(name = "number")
    private int number;
    @OneToMany(cascade = CascadeType.ALL,mappedBy = "week")
    private Set<Test> tests;
    @ManyToOne()
    @JoinColumn(name = "course_id",referencedColumnName = "id")
    private Course course;
    @ManyToOne()
    @JoinColumn(name = "user_id",referencedColumnName = "id")
    private User user;

}
