package com.model;

import javax.persistence.*;
import lombok.*;

import java.util.Set;

@Entity
@Table(name = "course")
@Getter
@Setter
public class Course {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(name="name")
    private String name;
    @Column(name="duration")
    private int duration;
    @Column(name="description")
    private String description;

    @OneToMany(cascade = CascadeType.ALL,mappedBy = "course")
    private Set<Week> weeks;

}
