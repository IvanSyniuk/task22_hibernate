package com.model;

import javax.persistence.*;
import lombok.*;
import java.util.Set;


@Entity
@Table(name = "test")
@Getter
@Setter
public class Test {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(name = "question")
    private String question;
    @OneToMany(cascade = CascadeType.ALL,mappedBy = "test")
    private Set<Answer> answers;
    @ManyToOne
    @JoinColumn(name = "modul_id", referencedColumnName = "id")
    private Modul modul;
    @ManyToOne()
    @JoinColumn(name = "week_id",referencedColumnName = "id")
    private Week week;


}
