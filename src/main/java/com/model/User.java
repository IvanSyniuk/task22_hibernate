package com.model;

import javax.persistence.*;
import lombok.*;

import java.util.Set;

@Entity
@Table(name="user")
@Getter
@Setter
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
private int id;
    @Column(name = "name")
    private String name;
    @Column(name = "surName")
    private String surName;

    @OneToMany(cascade = CascadeType.ALL,mappedBy = "user")
    private Set<Week> weeks;
    @OneToMany(cascade = CascadeType.ALL,mappedBy = "user")
    private Set<Message> messages;

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", surName='" + surName + '\'' +
                ", weeks=" + weeks +
                ", messages=" + messages +
                '}';
    }
}
