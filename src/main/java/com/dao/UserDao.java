package com.dao;

import com.model.User;

public interface UserDao extends GeneralDao<User,Integer> {

}
