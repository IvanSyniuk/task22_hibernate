package com.dao.impl;

import com.dao.UserDao;
import com.model.User;
import org.hibernate.Session;

import java.sql.SQLException;
import java.util.List;

public class UserDaoImpl implements UserDao {
    private Session session;
    public UserDaoImpl(Session session){
        this.session = session;
    }
    public List<User> findAll() throws SQLException {
        List <User>users =  session.createQuery("FROM User").list();
        return users;
    }

    public User findById(Integer id) throws SQLException {
        User user = session.get(User.class,id);
        return user;
    }

    public void create(User user) throws SQLException {
        session.getTransaction().begin();
       session.save(user);
        session.getTransaction().commit();
    }

    public void update(User user) throws SQLException {
        session.getTransaction().begin();
        session.update(user);
        session.getTransaction().commit();
    }

    public void delete(User user) throws SQLException {
        session.getTransaction().begin();
      session.remove(user);
      session.getTransaction().commit();
    }
}
