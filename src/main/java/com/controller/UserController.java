package com.controller;

import com.model.User;
import com.service.impl.UserServiceImpl;
import com.util.SessionUtil;
import org.hibernate.Session;

import java.sql.SQLException;
import java.util.Scanner;

public class UserController {

    static Scanner sc = new Scanner(System.in);
    private Session session;

    private UserServiceImpl userService;
    public UserController(){
        userService = new UserServiceImpl(SessionUtil.getSession());
    }


    public void chooseMethod(){
        try {
            switch (sc.nextInt()) {
                case 1:userService.create();break;
                case 2:
                    System.out.println("id:");
                    System.out.println(userService.findById(sc.nextInt()));break;
                case 3:System.out.println("id:");
                    userService.update(sc.nextInt());break;
                case 4:System.out.println("id:");
                userService.delete(sc.nextInt());break;
                case 5:userService.findAll();break;
            }
        }catch (SQLException e){
            e.printStackTrace();
        }
    }

}
