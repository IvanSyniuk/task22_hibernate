package com.service;

import com.model.User;

import java.sql.SQLException;

public interface UserService {
    void findAll() throws SQLException;

    User findById(Integer id) throws SQLException;

    void create() throws SQLException;

    void update(Integer id) throws SQLException;

    void delete(Integer id) throws SQLException;
}
