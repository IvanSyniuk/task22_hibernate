package com.service.impl;

import com.dao.impl.UserDaoImpl;
import com.model.User;
import com.service.UserService;
import org.hibernate.Session;

import java.sql.SQLException;
import java.util.List;
import java.util.Scanner;

public class UserServiceImpl implements UserService {
    static private Scanner sc = new Scanner(System.in);
    private Session session;
    private UserDaoImpl userDao;


    public UserServiceImpl(Session session) {
        this.session = session;
        userDao = new UserDaoImpl(session);
    }

    public void findAll() throws SQLException {
            userDao.findAll().forEach(System.out::println);
    }

    public User findById(Integer id) throws SQLException {
        return userDao.findById(id);
    }

    public void create() throws SQLException {
        User user = new User();
        System.out.println("Name:");
        user.setName(sc.next());
        System.out.println("Surname");
        user.setSurName(sc.next());
        userDao.create(user);
    }

    public void update(Integer id) throws SQLException {
   User user = findById(id);
        System.out.println("Name:");
    user.setName(sc.next());
        System.out.println("Surname");
    user.setSurName(sc.next());
        userDao.update(user);
    }

    public void delete(Integer id) throws SQLException {
        User user = findById(id);

        userDao.delete(user);
    }


}
